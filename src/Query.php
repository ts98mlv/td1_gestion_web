<?php
include "ConnectionFactory.php";

class Query
{

    private $db;
    private $table;
    private $select;
    private $conditions;

    public function __construct($table)
    {
        $this->db = ConnectionFactory::makeConnection(parse_ini_file(__DIR__ . "/../conf/db.conf.ini", true));
        $this->table = $table;
        $this->conditions = "";
        $this->select = "";
    }

    public static function table($table)
    {
        $query = new Query($table);
        return $query;
    }

    public function where($attr, $operande, $value)
    {
        if ($this->conditions == "")
            $condition = " WHERE ";
        else
            $condition = " AND ";

        if (is_int($value))
            $condition .= $attr . " " . $operande . " " . $value;
        else
            $condition .= $attr . " " . $operande . " '" . $value . "'";

        $this->conditions .= $condition;
        return $this;
    }

    public function get()
    {
        $sql = $this->select . " FROM " . $this->table . $this->conditions . ";";
        $query = $this->db->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        return $result;
    }

    public function select(...$attributs)
    {
        $select = "SELECT ";
        foreach ($attributs as $attribut) {
            $select .= $attribut . ", ";
        }

        // Sup dernier ', '
        $select = substr_replace($select, "", strlen($select) - 2);
        $this->select = $select;

        return $this;
    }

    public function delete($cond)
    {
        $query = $this->db->prepare("DELETE FROM " . $this->table . " WHERE " . $cond . ";");
        $query->execute();
    }

    public function insert($tabValues)
    {
        $attr = "";
        $val = "";
        foreach ($tabValues as $key => $value) {
            $attr .= "`" . $key . "`, ";

            if (is_int($value) || is_double($value))
                $val .= $value . ", ";
            else
                $val .= "'" . $value . "', ";
        }

        // Supprimer le dernier ', '
        $attr = substr_replace($attr, "", strlen($attr) - 2);
        $val = substr_replace($val, "", strlen($val) - 2);

        $insert = "INSERT INTO " . $this->table . "(" . $attr . ")" . " VALUES (" . $val . ");";

        $query = $this->db->prepare($insert);
        $query->execute();

        return $this->db->lastInsertId();
    }

}