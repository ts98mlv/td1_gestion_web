<?php


class ConnectionFactory
{
    private static $connexion;

    public function __construct()
    {
        ConnectionFactory::$connexion = null;
    }


    public static function makeConnection(array $conf){
        $host = $conf["host"];
        $plugin = $conf["plugin"];
        $db = $conf['dbname'];
        $user = $conf['user'];
        $pass = $conf['password'];

        $connection = new PDO("${plugin}:dbname=${db};host=${host}", $user, $pass);

        if(! isset(ConnectionFactory::$connexion))
            ConnectionFactory::$connexion = $connection;

        return ConnectionFactory::$connexion;
    }

    public static function getConnection()
    {
        return ConnectionFactory::$connexion;
    }
}