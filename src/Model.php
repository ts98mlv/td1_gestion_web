<?php


abstract class Model
{
    protected $tab;
    protected $db;

    public function __construct(Array $attributs)
    {
        $this->tab = $attributs;
    }

    public function __get($name)
    {
       if(isset($this->tab[$name]))
           return $this->tab[$name];
       else
           return null;
    }

    public function __set($name, $value)
    {
        $this->tab[$name] = $value;
    }

    public abstract function delete();

    public abstract function insert();

    public static abstract function all();

    public static abstract function find($critere = null, array $col = []);

    public static abstract function first($critere = null, array $col = []);

    public function has_many($nomTable,$nomForeignKey){
        $query = Query::table($nomTable);
        $query->where($nomForeignKey,'=',$this->id);
        $foreignModelArray = $query->select(['*'])->get();
        return $foreignModelArray;
    }

}